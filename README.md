# SlackPlasma
Get last KDE version from Alien Ktown, remove games and install.

Usage: 

1. Get packages from Alien repository (x86 or x86_64)

  > ./1_get.sh x86|x86_64

2. Remove games

  > ./2_rmgames.sh

3. Install packages

  > ./3_install.sh

